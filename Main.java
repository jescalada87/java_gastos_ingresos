

import java.util.List;
import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//empezamos
		/* He intentado tener el metodo main lo m�s minimalista posible y tratar de hacer
		 * todo el programa de manera modular. Que los datos se guarden en objetos y que sean
		 * las funciones las que ejecuten distintos m�todos de clase de las clases definidas.
		 * 
		 *Tambi�n he procurado que la lectura de datos no haya sido a trav�s de clases.
		 *
		 * He evitado declarar variables en el main que no fuesen necesarias y almacenar datos all�.
		 *
		 * Al hacerlo de manera tan modular, ir implementando nuevas funcionalidades es muy sencillo y 
		 * se empiezan a generar patrones, que evitan errores y favorecen la abstracci�n. 
		 * */
		
		
		
		/* Aqu� he creado una variable para que almecene un objeto tipo cuenta. 
		 * He estado muchas horas intentado pensar distintas formas de hacerlo para
		 * evitar una variable aqu�, pero el problema que me he encontrado es que el scope
		 * de un objeto, al menos lo que he podido experimentar no es visible desde funciones
		 * fuera del main. 
		 * As� que mi idea ha sido crear una cuenta que he ido metiendo por par�metros
		 * a las diferentes funciones. De esta manera las funciones conocen el objeto 
		 * creado y lo pueden manejar.
		 * 
		 *  En principio esto tambi�n ser�a para implementar en un futuro distintas cuentas
		 *  con otros usuarios.
		 *  
		 *  No s� si las soluciones que propongo son pr�cticas o son chapuzas. Ah� es donde
		 *  tengo las dudas.*/
		
		Cuenta cuentaunica = crearCuenta(); 
		
		
						
		menu(cuentaunica); //llamamos al menu que a su vez se imprime
		
			
		
	
		
		
			
   }	//fin del main
	
public static Cuenta crearCuenta() {
	
	Usuario usuario1 = new Usuario();
	
			/* Llamamos al m�todo que abre un scanner y guarda en memoria distintos datos
			 * vamos a�adiendo datos al objeto usuario, hacemos lo mismo con dni, edad y nombre
			 * */
	System.out.println("Introduce nombre de usuario");
	String nombreSc = pedirScannerStr();
	usuario1.setNombre(nombreSc);
	
	
	System.out.println("Introduce la edad del usuario");
	int edadSc = pedirScannerInt();
	usuario1.setEdad(edadSc);
		
	
	System.out.println("Introduce el Dni del usuario");
	String dniSc = pedirScannerStr();
	
	
	/* Con DNI adem�s tenemos que comprobar que cumple un formato.
	 * Validamos con el boolean que nos retorna el metodo de clase */
	while (!usuario1.setDNI(dniSc)){
	System.out.println("Dni introducido incorrecto\n"
			+ "Introduce el DNI del usuario v�lido");
	dniSc = pedirScannerStr();
	}
	
	System.out.println("Usuario creado correctamente");
	
	
	//una vez que ya hemos validaddo el DNI, podemos proceder a crear la cuenta
	
	Cuenta cuenta01 = new Cuenta(usuario1);
	//la cuenta se crea introduciendo por parametros un usuario
	
	
	return cuenta01;   //retornamos aqu� un objeto cuenta para que pueda ser almacenado en el main. Linea 42. 
	
	
}
	
public static void printMenu() {
	
	
			
	System.out.println("Realiza una nueva acci�n\n" + "1 Introduce un nuevo gasto \n" +
			"2 Introduce un nuevo ingreso \n" + 
			"3 Mostrar gastos \n" + 
			"4 Mostrar ingresos\n" + 
			"5 Mostrar saldo \n" +  "0 Salir\n");
	
}
public static void menu(Cuenta c) {
	
	printMenu(); //este metodo imprime el menu, lo dejamos dentro de este metodo por que siempre van paralelos. 
	
	String valorSc;
	 
	valorSc = pedirScannerStr();  //usamos scanner para almacenar dato de opci�n
	
	switch (valorSc) {	
	case "1":
		introduceGastos(c);
		break;
	case "2":				
		introduceIngresos(c);
		break;
	case "3":
		mostrarGastos(c);
		break;
	case "4":
		mostrarIngresos(c);
		break;
	case "5":
		mostrarSaldo(c);
		break;
	case "0":
		finalizar();		
		break;
	default:
		System.out.println("Opci�n no contemplada, por favor: ");
		menu(c); //retornamos al men�, imprimiendolo y pidiendo opcion, funcion recursiva?
					
	} //fin switch 
	
	
	
	
}
public static void introduceIngresos(Cuenta c) {
	
	
	
	System.out.println("Introduce la descripci�n");
	String descrip = pedirScannerStr();
	System.out.println("Introduce la cantidad");
	double ingre = pedirScannerDoub();
	
	c.addIngresos(descrip, ingre); //sumamos un ingreso en el campo saldo del objeto cuenta01
	System.out.println("Saldo restante: " + c.getSaldo());
	menu(c);
		
}
public static void introduceGastos(Cuenta c) {
	
		
	
	try {
		
		System.out.println("Introduce la descripci�n");
		String descrip = pedirScannerStr();
		System.out.println("Introduce la cantidad");
		double gas = pedirScannerDoub();
		
				
		if (gas > c.getSaldo()) {
		
		throw new GastoException();
		
		} else {
			
			c.addGastos(descrip, gas);
			System.out.println("Saldo restante: " + c.getSaldo()); //si en este tipo de situaciones hacemos mostrarSaldo(c); tendremos problemas para salir del programa
			menu(c);
		}
		
	} catch (GastoException ex) { //este bloque se ejecutara si el try lanza una excepcion
		
		
		introduceGastos(c);
		
		
	}
	
	/*Igual que la funcion de ingresos pero implementamos la clase gastoExcepction que arroja
	 * un error en caso de que gasto sea mayor que el saldo
	 * Hasta que no se valide, el catch nos hace retornar al principio. */
	
	//   System.out.println("Saldo restante: " + c.getSaldo());
	//  menu(c); //validado y a�adido el gasto, volvemos a menu
		
}
public static void mostrarGastos(Cuenta c) {
	
	System.out.println(c.getGastos());
	menu(c);
	
}
public static void mostrarIngresos(Cuenta c) {
	
	System.out.println(c.getIngresos());
	menu(c);
	
}
/*Estas funciones de scanner las tuve que crear porque ten�a diversos problemas con 
 * los scanners, por temas de buffer, he probado distintas soluciones y buscado por SO. 
 * Pero nada de lo que prob� funcionaba. 
 * 
 * No estoy seguro si con un gen�rico hubiera necesitado solo una funci�n. Tampoco s� 
 * si es buena idea o no, porque de ser buena idea, ya estar�a dise�ado as� el propio scanner. 
 * Lo que s� lo he visto es muy pr�ctico, porque solo tienes que llamar a una funci�n y en 
 * una l�nea de c�digo tienes almacenados los datos y el scanner se crea y destruye en 
 * el momento (creo?).  
 * */
public static int pedirScannerInt() {
	
	Scanner sc = new Scanner(System.in);
	
		
	return sc.nextInt();
	
}
public static String pedirScannerStr() {
	
	Scanner sc = new Scanner(System.in);
			
	return sc.nextLine();
	
}
public static Double pedirScannerDoub() {
	
	Scanner sc = new Scanner(System.in);
			
	return sc.nextDouble();
	
}
public static void mostrarSaldo(Cuenta c)
{
	System.out.println("Saldo restante: " + c.getSaldo());
	menu(c);
}
public static void finalizar() {
	
		System.out.println("Fin del programa. \n"
			+ "Gracias por utilizar la aplicaci�n."); //fin del programa
}
} //fin class programa