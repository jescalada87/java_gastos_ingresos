

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Usuario {
	//Constructor
	
	public Usuario() {};
	
	// Aqu� los atributos
	
	private String nombre;
	
	private int edad;
	
	private String DNI;
	
	
	// Aqu� los setters y getters ----------
	public void setNombre(String nombre) {
		
		this.nombre = nombre;
	}
	
	public String getNombre() {
		
		return nombre; 
		
	}
	
	public void setEdad(int edad) {
		
		this.edad = edad;
	}
	
	public int getEdad() {
		
		return edad;
		
	}
	
	public String getDNI() {
		
		return DNI;
	}
	
	
	/* Metodo toString para mostrar la info*/
	@Override
	public String toString() {
				
		return "Nombre, edad y DNI: " + nombre + " " + edad + " " + DNI + "\n"; 
		
	}
	
	/* Metodo que introduce el DNI y comprueba si el DNI es correcto */
	
	public boolean setDNI(String DNI) { 
		
					
		Pattern p = Pattern.compile("[0-9]{8}?[a-zA-Z]|[0-9]{8}?[-][a-zA-Z]");
		
		Matcher m = p.matcher(DNI);
		
		if (m.matches()) {
			
			this.DNI = DNI;  //si coincide con el patr�n, podemos guardarr ya el dni. 
			return m.matches();
		}
						
			return m.matches();
		//retorna booleano que usaremos despu�s en el flujo del programa
	}
	
}
