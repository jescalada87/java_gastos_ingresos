

public abstract class Dinero {
	protected double dinero;	
	protected String description;
	
	/* Aqu� es curioso porque los atributos los hab�a declarado inicialmente
	 * como private, y el IDE me ha obligado a hacerlos protected, y 
	 * eso ha hecho que haya entendido mejor por qu� se necesita usar
	 * protected...porque es una clase de la que se hereda.
	 * 
	 * Esta clase adem�s de ahorrar c�digo creo que servir�a para tener ciertas validaciones
	 * por ejemplo para evitar que un dato de dinero sea negativo.
	 * 
	 * */
	
	public double getDinero() {
		return dinero;
	}
	public void setDinero(double dinero) {
		this.dinero = dinero;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}