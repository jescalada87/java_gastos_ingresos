

public class Ingreso extends Dinero {
	
	public Ingreso (double ingreso, String description) {
		
		this.dinero=ingreso;   //hace referencia a la clase dinero (atrib dinero)
		this.description = description; //idem
		
	}
	
	public String toString() {
		
		return this.getDescription() + ", cantidad:" + this.getDinero() + "�\n";   
		
	}
}

