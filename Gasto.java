

public class Gasto extends Dinero{
	public Gasto (double gasto, String description)
	{
		dinero=gasto;   //hace referencia a la clase dinero, el atributo dinero
		this.description=description; //idem
		
	}
		
	public String toString() {
		
		return this.getDescription() + ", cantidad:" + this.getDinero() + "�\n";   
		
	}
	
}