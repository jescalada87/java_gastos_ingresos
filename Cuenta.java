


	
import java.util.ArrayList;
import java.util.List;

public class Cuenta {
	
	private double saldo = 0;
    private Usuario usuario;
    /* Ojo porque es un objeto, no una variable
     * normal, esto hace por ejemplo que si 
     * lanzas un getUsuario, nos lea las
     * propiedades el usuario en s�,
     * que son nombre, dni y edad */
    private List<Gasto> gastos = new ArrayList<Gasto>();
    private List<Ingreso> ingresos = new ArrayList<Ingreso>();
    
    
       
    
    public Cuenta(Usuario usuario) {
        /*La cuenta recibe por parametro otro
         * objeto tipo usuario*/ 
    	
       this.usuario = usuario;
        
    }
 
    // setters y getters
    
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
     
    
    public double getSaldo() {
        return saldo;
    }
 
  
    public void setUsuario(Usuario usuario) {
        this.usuario=usuario;
    }
     
    public Usuario getUsuario() {
        
    	/*Si hacemos un cuenta.getUsuario nos lanzar�
    	 * el resiltado de usuario.toString*/
    	return usuario;
    }
     
        
    // Muestra gastos
    public List<Gasto> getGastos() {
        return gastos;
    }
     
         
    // Muestra ingresos
    public List<Ingreso> getIngresos() {
        return ingresos;
    }
     
    // suma ingresos
    public double addIngresos(String description, double cantidad) {
        
    	Ingreso ing1 = new Ingreso(cantidad, description);
    	
    	    	
    	this.saldo += cantidad;
    	
    	this.ingresos.add(ing1);
    	
    	
    	
    	/* Aqui creamos el objeto ingreso con los datos
    	 * que vienen de la funci�n que a su vez vienen del scanner.
    	 * 
    	 * Con ese objeto ingreso rellenamos la lista ingresos, que
    	 * contendr� todos los ingresos
    	 * 
    	 * Tambi�n tenemos que cambiar el valor de saldo. */
    	
    	return cantidad; //esta funcion ser�a mejor que fuese void, pero el problema lo especifica as�
    	
    }
     
    // agrega gastos [funciona igual que la anterior]
    public double addGastos(String description, double cantidad) {
        
    	Gasto gas1 = new Gasto(cantidad, description);
    	
    	this.saldo -= cantidad;
        this.gastos.add(gas1);
    	
    	
    	return cantidad;
    }
     
    public String toString() {
        
    	return this.getUsuario() + " Saldo " + this.saldo + " � \n";
    /* Con este toString lanzamos todos los datos del usuario 
     * lo que son su edad, dni y nombre*/
    }
	
}
